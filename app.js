'use strict';

let express = require('express');
let bodyParser = require('body-parser');
let path = require('path');
let PDFParser = require('pdf2json');
let gm = require('gm');
let request = require('request');
let fs = require('fs');
let elasticsearch = require('elasticsearch');
let cryptiles = require('cryptiles');
let config = require('dotenv').config({silent: true});

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => res.sendFile('/public/index.html'));

app.post('/pdf', (req, res) => {

  let accessToken = req.body.access_token;

  if (accessToken === undefined || accessToken != process.env.ACCESS_TOKEN) {
    res.status(401).json({
      'message': 'Access denied.'
    });
    return;
  }

  let url = req.body.url;
  console.log('URL: ' + url);

  let fileId = cryptiles.randomString(5) + '_' + Date.now();

  // var imgDestination = './public/img/';
  let destination = process.env.TMP_DIR + fileId + '.pdf';
  console.log('Tmp file: ' + destination);

  let file = fs.createWriteStream(destination);

  request(url)
    .pipe(file)
    .on('error', (error) => console.log('Request Error: ' + error));

  file.on('finish', () => {
    console.log('File download finished');
    file.close();

    createThumb(fileId)
      .then(() => parseFile(fileId))
      .then((content) => {
        fs.unlink(destination, () => console.log('Temp file removed.'));
        res.json({
          message: 'PDF file successfully parsed.',
          data: content
        });
      })
      // .catch(console.error);
      .catch((error) => {
        console.error;
        res.status(500).json({
          message: 'There was an issue parsing the PDF.',
          details: error
        });
      });

  });

});

let createThumb = (fileId) => {
  return new Promise((resolve, reject) => {
    console.log('Thumbnail generation triggered.');
    let file = process.env.TMP_DIR + fileId + '.pdf';
    gm(file)
      .thumb(
        200, // Width
        200, // Height
        process.env.IMG_DIR + fileId + '.png', // Output file name
        90, // Quality from 0 to 100
        (error, stdout, stderr, command) => {
          if (!error) {
            console.log('Thumbnail created.');
            resolve(fileId);
          } else {
            reject(error);
          }
        }
      );

  });
};


let parseFile = (fileId) => {
  return new Promise((resolve, reject) => {
    console.log('Start PDF crawling.');

    let pdfParser = new PDFParser(this, 1);

    pdfParser.on("pdfParser_dataError", (errData) => reject(errData.parserError));

    pdfParser.on("pdfParser_dataReady", (pdfData) => {
      console.log('PDF data crawled.');
      let data = {
        thumbnail: process.env.IMG_DIR + fileId + '.png',
        text: pdfParser.getRawTextContent()
      };
      resolve(data);
    });

    pdfParser.loadPDF(process.env.TMP_DIR + fileId + '.pdf');
  });
};


app.listen(3030, () => console.log('HayPdf listening on port 3030!'));
